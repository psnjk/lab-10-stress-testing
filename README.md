# Lab10 -- Stress testing 


## Homework

**Lab is counted as done, if pipelines are passing. and tests are developped**

Link - ```https://script.google.com/macros/s/AKfycby2pSIiyCyM1AlYJ-2EkycGPjNm-zGdwuduE93ipTX6T6f6ZbJ-Vp0wGwTxsOc11C8e/exec```
Artillery config - ```homework.yaml```

Tested services:
- ```getSpec```
- ```calculatePrice``` -commented in config

Report in pipeline is generated as artifact

Screenshots:

![](screenshots/report1.png)
![](screenshots/report2.png)
![](screenshots/report3.png)
![](screenshots/report4.png)

Screenshots of process:

![](screenshots/Screenshot_from_2022-04-26_12-10-52.png)
![](screenshots/Screenshot_from_2022-04-26_12-11-08.png)
![](screenshots/Screenshot_from_2022-04-26_12-11-17.png)
![](screenshots/Screenshot_from_2022-04-26_12-11-32.png)
